#!/usr/bin/python
"""
multiple MySQL sources: polled
multiple(?) MySQL outputs: separate thread
share-count OR generation-producer server
	keep track of last-found-block from bitcoind perspective
"""

from anynumber import Number, Naught
import collections
from copy import deepcopy
from datetime import datetime
from hashlib import sha256
import json
import logging
import SocketServer
import sys
import threading
import time
import traceback
import types
import warnings

AdjShareRes = 0x1000000000000

logging.basicConfig(stream=sys.stderr, level=logging.INFO)

timmill = 1.318359375

def ignoreerror(*L, **kw):
	etype = kw.get('etype', Exception)
	for Li in L:
		try:
			Li()
		except etype:
			pass

def parsetime(t):
	if isinstance(t, datetime):
		return int(time.mktime(t.timetuple()))
	return t

class QueueEmpty(Exception):
	pass
class QueueFull(Exception):
	pass

class Queue:
	loopdelay = timmill / 0x1000
	
	def __init__(self):
		self._list = []
		self._lock = threading.Lock()
		self._event = threading.Event()
		self.maxLimit = 0x10000
		self.maxWarn = 0x4000
	
	def put(self, item, block = True, timeout = None):
		try:
			self._lock.acquire() ; locked = True
			
			sz = len(self._list)
			if sz >= self.maxLimit:
				if not block:
					raise QueueFull
				logging.warn('%s overflow at 0x%x, blocking add until it shrinks' % (self, sz))
				
				while True:
					locked = False ; self._lock.release()
					time.sleep(timmill)
					self._lock.acquire() ; locked = True
					
					sz = len(self._list)
					if sz < self.maxLimit:
						break
					logging.warn('... %s still waiting (0x%x)' % (self, sz))
				logging.info('%s size reduced to 0x%x, finishing add' % (self, sz))
			self._list.append(item)
			self._event.set()
			sz += 1
			if not sz % self.maxWarn:
				logging.warn('%s growing very large (0x%x)' % (self, sz))
		finally:
			if locked:
				self._lock.release()
	
	def get(self, block = True, timeout = None):
		if not timeout is None:
			timeout = time.time() + timeout
		while True:
			with self._lock:
				try:
					rv = self._list.pop(0)
				except IndexError:
					pass
				else:
					if not len(self._list):
						self._event.clear()
					return rv
			if block:
				youhavegain = False
				while True:
					lto = min(self.loopdelay, timeout - time.time() if timeout else ())
					if lto < 0:
						break
					if self._event.wait(lto):
						youhavegain = True
						break
				if youhavegain:
					continue
			
			raise QueueEmpty
	
	def __len__(self):
		with self._lock:
			return len(self._list)

def byteFlip(s):
	x = ''
	for i in xrange(0, len(s), 4):
		x += s[i + 3] + s[i + 2] + s[i + 1] + s[i]
	return x

def Solution2HTime(s):
	return int(s[68:72].encode('hex'), 0x10)

def Solution2Bits(s):
	return s[72:76]

def Solution2BlkHash(solution):
	solution = solution[:80]
	solution = byteFlip(solution)
	sharehash = sha256(sha256(solution).digest()).digest()
	sharehash = sharehash[::-1]
	return sharehash

def Bits2Target(bits):
	return int(bits[1:].encode('hex'), 0x10) * 2**(8*(int(bits[0].encode('hex'), 0x10) - 3))

PoolShareTarget = 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF

def Target2AdjShare(target, sharetarget = PoolShareTarget):
	return int(AdjShareRes * target / sharetarget)

block_hash_dict = {}

class ShareTypes(object):
	All = True
	Absolute = 0
	Adjusted = 1
	
	@staticmethod
	def getInitial(sharetype):
		if sharetype is ShareTypes.All:
			return [0, 0]
		elif sharetype is ShareTypes.Absolute:
			return 0
		elif sharetype is ShareTypes.Adjusted:
			return 0

class OutOfOrderError(IndexError):
	pass

def _IndexShare(o, kwa):
	server = kwa['server']
	sharetime = kwa['time']
	shareid = kwa['id']
	# Index the share
	if server not in o._serverData:
		o._serverData[server] = [(), None, (), None]
	sd = o._serverData[server]
	if shareid < sd[0]: sd[0] = shareid
	if shareid > sd[1]: sd[1] = shareid
	if sharetime < sd[2]: sd[2] = sharetime
	if sharetime > sd[3]: sd[3] = sharetime
	if sharetime > o.endTime:
		o.endTime = sharetime

class ShareCheckpoint:
	def __init__(self, startTime, prevcp = None, nextcp = None):
		self._sharesAbs = {}
		self._sharesAdj = {}
		self._serverData = {}
		self.startTime = startTime
		self.endTime = startTime
		self.prevcp = prevcp
		self.nextcp = nextcp
		self.volatile = True
		if prevcp:
			if prevcp.nextcp:
				raise NotImplementedError('Tried to add a checkpoint in the middle of a chain')
			if prevcp.startTime >= startTime:
				raise NotImplementedError('Tried to add a checkpoint going back in time (%d >= %d)' % (nextcp.startTime, startTime))
			prevcp.nextcp = self
		if nextcp:
			if nextcp.prevcp:
				raise NotImplementedError('Tried to add a checkpoint in the middle of a chain')
			if nextcp.startTime < startTime:
				raise NotImplementedError('Tried to add a checkpoint going back in time (%d < %d)' % (nextcp.startTime, startTime))
			nextcp.prevcp = self
	
	def save_to_dat(self):
		d = {}
		d['startTime'] = self.startTime
		d['endTime'] = self.endTime
		d['serverData'] = deepcopy(self._serverData)
		d['sharesAbs'] = self._sharesAbs.copy()
		d['sharesAdj'] = self._sharesAdj.copy()
		return d
	
	def load_from_dat(self, d):
		if self._serverData:
			raise RuntimeError('Cannot load into an active ShareCheckpoint')
		assert self.startTime == d['startTime']
		self.endTime = d['endTime']
		self._serverData = d['serverData']
		self._sharesAbs = d['sharesAbs']
		self._sharesAdj = d['sharesAdj']
		self.volatile = False
	
	def _lastShareId(self, server):
		# NOTE: only guaranteed to be >= last share, not ==
		if server not in self._serverData:
			return 0
		return self._serverData[server][1]
	
	def addShare(self, **kwa):
		logging.debug('%s receiving share: %s' % (self, kwa))
		
		sharetime = kwa['time']
		if sharetime < self.startTime:
			raise OutOfOrderError('Tried to add a share earlier than checkpoint %d: %s' % (self.startTime, kwa))
		
		_IndexShare(self, kwa)
		
		if kwa['our_result'] != 'Y':
			# Reject invalid shares
			logging.debug('... skipping (our_result != \'Y\')')
			return
		
		# Actually count the share
		username = kwa['username']
		solution = kwa['solution']
		target = Bits2Target(Solution2Bits(solution))
		if username not in self._sharesAbs:
			self._sharesAbs[username] = 0
			self._sharesAdj[username] = 0
		self._sharesAbs[username] += 1
		self._sharesAdj[username] += Target2AdjShare(target)
	
	def _removeShare(self, **kwa):
		# NOTE: this leaves endTime WRONG
		logging.debug('%s removing share: %s' % (self, kwa))
		
		# De-index
		server = kwa['server']
		shareid = kwa['id']
		sd = self._serverData[server]
		if sd[0] == shareid:
			if sd[1] == shareid:
				del self._serverData[server]
			else:
				sd[0] += 1
		else:
			if sd[1] == shareid:
				sd[1] -= 1
		
		if kwa['our_result'] != 'Y':
			# Reject invalid shares
			logging.debug('... skipping (our_result != \'Y\')')
			return
		
		# Actually un-count the share
		username = kwa['username']
		solution = kwa['solution']
		target = Bits2Target(Solution2Bits(solution))
		if self._sharesAbs[username] == 1:
			del self._sharesAbs[username]
			del self._sharesAdj[username]
		else:
			self._sharesAbs[username] -= 1
			self._sharesAdj[username] -= Target2AdjShare(target)
	
	def _countShares(self, sharetype, username = None, keycheck = True):
		if sharetype is ShareTypes.All:
			pass
		elif sharetype is ShareTypes.Absolute:
			d = self._sharesAbs
		elif sharetype is ShareTypes.Adjusted:
			d = self._sharesAdj
		else:
			raise ValueError('Unknown share type "%s"' % (sharetype,))
		if username is True:
			if sharetype is ShareTypes.All:
				rv = {}
				for (k, v) in self._sharesAbs.iteritems():
					rv[k] = (v, self._sharesAdj[k])
				return rv
			return d
		if username is None:
			rv = ShareTypes.getInitial(sharetype)
			if sharetype is ShareTypes.All:
				for (k, v) in self._sharesAbs.iteritems():
					rv[0] += v
					rv[1] += self._sharesAdj[k]
				return rv
			for (k, v) in d.iteritems():
				rv += v
			return rv
		if keycheck and username not in self._sharesAbs:
			return ShareTypes.getInitial(sharetype)
		if sharetype is ShareTypes.All:
			return (self._sharesAbs[username], self._sharesAdj[username])
		return d[username]

class CheckpointNeededError(ValueError):
	pass

class ShareCheckpointIterator:
	def __init__(self, timeline, startTime, endTime = None):
		if startTime not in timeline.cp:
			raise CheckpointNeededError('Cannot start counting shares at a time (%d) without checkpoint' % (startTime,))
		if endTime:
			if endTime + 1 not in timeline.cp:
				raise CheckpointNeededError('Cannot end counting shares at a time (%d) without checkpoint immediately following' % (endTime,))
			if endTime >= timeline.endTime:
				raise CheckpointNeededError('Not certainly caught up enough to end (%d) at this time yet' % (endTime,))
			if timeline.cp[endTime + 1].prevcp.volatile:
				raise CheckpointNeededError('End checkpoint (%d) is still being built' % (endTime,))
		else:
			endTime = timeline.lastcp.endTime
		self.cp = timeline.cp[startTime]
		self.endTime = endTime
	
	def __iter__(self):
		return self
	
	def next(self):
		rv = self.cp
		
		if not rv:
			raise StopIteration
		
		self.cp = self.cp.nextcp
		if self.cp and self.cp.startTime > self.endTime:
			self.cp = None
		
		return rv

class ShareTimeline:
	def __init__(self, name, startTime):
		self.name = name
		self.logger = logging.getLogger(name)
		self.firstcp = ShareCheckpoint(startTime)
		self.firstcp.volatile = False
		self.lastcp = self.firstcp
		self.cp = {startTime: self.firstcp}
		self.hackBack = None
		self._serverData = {}
		self.endTime = None
	
	def save_to_dat(self):
		# FIXME: global lock needed
		d = []
		cp = self.firstcp
		while cp:
			d.append(cp.save_to_dat())
			cp = cp.nextcp
		d = {'cp':d}
		d['block_hash_dict'] = deepcopy(block_hash_dict)
		return d
	
	def get_solid_dat(self):
		a = self.save_to_dat()
		b = self.save_to_dat()
		assert a == b
		return a
	
	def write_to_file(self, f):
		import pickle
		pickle.dump(self.get_solid_dat(), f)
	
	def load_from_dat(self, d):
		if self._serverData:
			raise RuntimeError('Cannot load a file into an active ShareTimeline')
		cp = None
		for di in d['cp']:
			startTime = di['startTime']
			newcp = ShareCheckpoint(startTime, prevcp=cp)
			newcp.load_from_dat(di)
			if not cp: self.firstcp = newcp
			cp = newcp
			self.cp[startTime] = newcp
			if di['endTime'] > self.endTime: self.endTime = di['endTime']
			for (server, nsd) in di['serverData'].iteritems():
				if server not in self._serverData:
					self._serverData[server] = [(), None, (), None]
				sd = self._serverData[server]
				if nsd[0] < sd[0]: sd[0] = nsd[0]
				if nsd[1] > sd[1]: sd[1] = nsd[1]
				if nsd[2] < sd[2]: sd[2] = nsd[2]
				if nsd[3] > sd[3]: sd[3] = nsd[3]
		self.lastcp = cp
		global block_hash_dict
		block_hash_dict = d['block_hash_dict']
	
	def load_from_file(self, f):
		import pickle
		self.load_from_dat(pickle.load(f))
	
	def addShare(self, *args, **kwargs):
		self.logger.debug('"%s" receiving share: %s' % (self.name, kwargs))
		
		server = kwargs['server']
		shareid = kwargs['id']
		# Sanity check
		lsid = self._serverData.get(server, (None, 0))[1]
		if lsid >= shareid:
			raise OutOfOrderError('Tried to add "%s" 0x%x after 0x%x' % (server, shareid, lsid))
		_IndexShare(self, kwargs)
		
		cp = self.lastcp
		sharetime = kwargs['time']
		if kwargs['upstream_result'] == 'Y' or kwargs['id'] == 1:
			# This is a valid block. Declare a checkpoint.
			# Also add a checkpoint for the first share in a source db
			solution = kwargs['solution']
			sharehtime = Solution2HTime(solution)
			kwargs['ntime'] = sharehtime
			# TODO: order of checkpoints won't matter later
			# TODO: hackback can be optimized for this scenario...
			if sharetime < 1311972179 and sharehtime < sharetime:
				self.addCheckpoint(sharehtime)
			cp = self.addCheckpoint(sharetime)
			if sharetime < 1311972179 and sharehtime > sharetime:
				self.addCheckpoint(sharehtime)
			
			# Index its hash too, so we can provide time lookups...
			self.logger.debug('... hashing valid block')
			block_hash = Solution2BlkHash(solution).encode('hex')
			self.logger.info('New block %s found at time %d (ntime %d)' % (block_hash, sharetime, sharehtime))
			block_hash_dict[block_hash] = kwargs
		elif sharetime < cp.startTime:
			while sharetime < cp.startTime:
				cp = cp.prevcp
				self.logger.debug('... backdating to previous checkpoint %s' % (cp,))
		return cp.addShare(*args, **kwargs)
	
	def lastShareId(self, server):
		if server not in self._serverData:
			return 0
		return self._serverData[server][1]
	
	def addCheckpoint(self, atTime):
		if atTime in self.cp:
			# Already exists, so just return it
			return self.cp[atTime]
		
		backtrack = self.lastcp.endTime >= atTime
		if backtrack and not self.hackBack:
			raise ValueError('Tried to add a checkpoint already in the past/present (%d < %d)' % (atTime, self.lastcp.endTime))
		
		self.logger.info('Adding checkpoint at %d on "%s"' % (atTime, self.name))
		
		prevcp = self.lastcp
		while prevcp and prevcp.startTime > atTime:
			prevcp = prevcp.prevcp
		nextcp = prevcp.nextcp if prevcp else self.firstcp
		
		# Break the links, so we can insert between them
		if prevcp: prevcp.nextcp = None
		if nextcp: nextcp.prevcp = None
		
		cp = ShareCheckpoint(atTime, prevcp=prevcp, nextcp=nextcp)
		if not prevcp: self.firstcp = cp
		if not nextcp: self.lastcp  = cp
		self.cp[atTime] = cp
		if backtrack:
			kwa = {}
			if nextcp:
				kwa['endTime'] = nextcp.startTime
			self.hackBack(atTime, prevcp, cp, **kwa)
		cp.volatile = False
		
		return cp
	
	def countShares(self, sharetype, startTime = 0, endTime = None, username = None):
		# username can be:
		#     None (count all shares)
		#     True (return a dict of username:share)
		#     string (count only their shares)
		def merge(a, b):
			return a + b
		if sharetype is ShareTypes.All:
			def merge(a, b, merge=merge):
				for i in xrange(len(b)):
					a[i] = merge(a[i], b[i])
				return a
		if username is True:
			rv = {}
			def merge(a, b, merge=merge):
				for (k, v) in b.iteritems():
					a[k] = merge(a[k], v) if k in a else v
				return a
		else:
			rv = ShareTypes.getInitial(sharetype)
		for cp in ShareCheckpointIterator(self, startTime, endTime):
			try:
				rvm = cp._countShares(sharetype, username=username)
			except KeyError:
				continue
			rv = merge(rv, rvm)
		return rv

class DBThread(threading.Thread):
	# DB-API 2.0 abstract base class
	ErrorDelay = timmill * 0x10
	_paramopts = {
		'qmark': '?',
		'format': '%s',
		'pyformat': '%s',
	}
	
	def __init__(self, name, dbmod, dbconnect):
		threading.Thread.__init__(self)
		self.daemon = True
		self.name = name
		self.logger = logging.getLogger(name)
		self.dbmod = dbmod
		self.OperationalError = dbmod.OperationalError
		self.dbconnect = dbconnect
		self._inqueue = Queue()
		self._todo = None
		self.param = self._paramopts[dbmod.paramstyle]
		self._db = None
	
	def run(self):
		while True:
			self._eachtime()
	
	def _eachtime(self):
		db = None
		try:
			db = self._db
			if db is None:
				self.logger.info('Connecting to "%s"...' % (self.name,))
				db = self.dbconnect()
				self._db = db
			dbc = db.cursor()
			if self._todo:
				self._todo(dbc)
				self._todo = None
			else:
				self.dbact(dbc)
			self.dbwait()
			dbc.close()
		except Exception:
			self.logger.error('Error in "%s"' % (self.name,))
			self.logger.error(traceback.format_exc())
			ignoreerror(lambda: dbc.close())
			ignoreerror(lambda: db.close())
			self._db = None
			time.sleep(self.ErrorDelay)
	
	def dbwait(self, timeout = None):
		if self._todo:
			return
		try:
			self._todo = self._inqueue.get(block=True, timeout=timeout)
		except QueueEmpty:
			pass
	
	def dbact(self, dbc):
		pass
	
	def _dbdo_f(self, stmt, param, callback, commit, retry, rv, dbc):
		self.logger.debug('"%s" execute: <%s> %s' % (self.name, stmt, param))
		try:
			erv = dbc.execute(stmt, param)
			if commit:
				self._db.commit()
			dbr = dbc
			if dbc.rowcount == 0:
				dbr = ()
			if callback:
				dbr = callback(erv, dbr)
			else:
				try:
					dbr = dbr.fetchall()
				except Exception:
					dbr = erv
			rv.put((0, dbr), block=True)
		except self.OperationalError if retry else None:
			# Needed to force reconnect
			self._inqueue.put(lambda dbc: self._dbdo_f(stmt, param, callback, commit, retry, rv, dbc), block=True)
			raise
		except BaseException, e:
			rv.put((1, e, traceback.format_exc()), block=True)
	
	def dbdo(self, stmt, param, callback = None, commit = False, retry = False):
		stmt = stmt % ((self.param,) * stmt.count('%s'))
		rv = Queue()
		self._inqueue.put(lambda dbc: self._dbdo_f(stmt, param, callback, commit, retry, rv, dbc), block=True)
		rv = rv.get(block=True)
		if rv[0]:
			self.logger.error(rv[2])
			raise rv[1]
		return rv[1]
	
	def wakeup(self):
		self._inqueue.put(lambda x:None, block=True)

class DBSource(DBThread):
	# Polls a DB-API 2.0 interface for shares
	OkDelay = timmill / 2
	
	def __init__(self, name, dbmod, dbconnect, output, server):
		DBThread.__init__(self, name, dbmod, dbconnect)
		self.output = output
		self.server = server
		self.lastid = None
		self._e = False
	
	def dbwait(self):
		delay = self.OkDelay
		if self.lastid is None:
			delay = None
		DBThread.dbwait(self, delay)
	
	def dbact_i(self, dbc):
		# Need to prevent main loop from moving on without us... TODO FIXME
		dbc.execute('select * from shares where id > %s order by id asc limit 4096' % (self.param,), (self.lastid,))
		dbr = dbc
		if dbc.rowcount == 0:
			return
		fa = tuple(str(x[0]) for x in dbc.description)
		for row in dbr:
			rd = dict( (fa[i], row[i]) for i in xrange(len(fa)) )
			if rd['id'] <= self.lastid:
				raise OutOfOrderError('"%s" got shareid 0x%x after 0x%x' % (self.name, rd['id'], self.lastid))
			rd['time'] = parsetime(rd['time'])
			rd['solution'] = str(rd['solution'])
			rd['server'] = self.server
			self.logger.debug('Fetched "%s"/0x%x' % (self.name, rd['id']))
			self.output.put(rd, block=True)
			self.lastid = rd['id']
	
	def dbact(self, dbc):
		if self.lastid is None:
			return
		if len(self.output) > 0x1000:
			msg = 'NOT fetching updates from "%s" because output has 0x%x already' % (self.name, len(self.output))
			if self._e:
				self.logger.debug(msg)
			else:
				self.logger.warn(msg)
				self._e = True
			return
		self._e = False
		self.logger.debug('Fetching updates from "%s" (lastid=%d)' % (self.name, self.lastid))
		self.dbact_i(dbc)
		self.logger.debug('Done with updates from "%s"' % (self.name,))

class DBOutput(DBThread):
	def __init__(self, name, dbmod, dbconnect):
		DBThread.__init__(self, name, dbmod, dbconnect)
		self.lastid = {}
	
	def lastShareId(self, server):
		if server in self.lastid:
			return self.lastid[server]
		dbr = self.dbdo('select id from shares where server = %s order by id desc limit 1', (server,))
		dbr = dbr[0][0] if dbr else 0
		self.lastid[server] = dbr
		return dbr
	
	def addShare(self, **info):
		self.logger.debug('"%s" receiving share: %s' % (self.name, info))
		server = info['server']
		shareid = info['id']
		if shareid <= self.lastShareId(server):
			raise OutOfOrderError('Trying to add a share out-of-order: 0x%x does not follow 0x%x' % (shareid, self.lastid))
		fs = ','.join(info.keys())
		qs = ','.join(('%s',) * len(info))
		va = info.values()
		self.dbdo('insert into shares (%s) values (%s)' % (fs, qs), va)
		self.lastid = shareid

class UserDBDict(DBThread):
	def __init__(self, name, dbmod, dbconnect):
		DBThread.__init__(self, name, dbmod, dbconnect)
		self._d = {}
		self._r = {}
		self.loaded = False
	
	def dbwait(self):
		return
	
	def dbact_i(self, dbc):
		# Load initial user db
		dbc.execute('select id, username from users')
		dbr = dbc
		if dbr.rowcount == 0:
			self.loaded = True
			return
		
		self.logger.info('Loading users from database (%s)...' % (dbr.rowcount,))
		for (userid, username) in dbr:
			self._d[username] = userid
			self._r[userid] = username
		self.logger.info('Done loading %d users' % (len(self._d),))
		self.loaded = True
	
	def dbact(self, dbc):
		self.dbact_i(dbc)
		self.dbact_i = None
		self.dbwait = types.MethodType(DBThread.dbwait, self, self.__class__)
		self.dbact  = types.MethodType(DBThread.dbact , self, self.__class__)
	
	def get(self, username):
		if not self.loaded:
			self.logger.warn('Trying to lookup a username before UserDB loaded')
			while not self.loaded:
				time.sleep(timmill)
		if username in self._d:
			return self._d[username]
		def f(self, dbr):
			with warnings.catch_warnings():
				warnings.filterwarnings('ignore', 'DB-API extension cursor.lastrowid used')
				lrid = None
				try:
					lrid = dbr.lastrowid
				except Exception:
					pass
				return lrid
		lrid = self.dbdo('insert into users (username) values (%s)', (username,), callback=f, commit=True, retry=True)
		if not lrid:
			lrid = self.dbdo('select id from users where username = %s limit 1', (username,), retry=True)[0][0]
		self._d[username] = lrid
		self._r[lrid] = username
		self.logger.debug('Created userid %d for "%s"' % (lrid, username))
		return lrid
	
	def _rewrite_db(self):
	    for (uid, username) in self._r.iteritems():
		self.dbdo('insert into users (id, username) values (%s, %s)', (uid, username))
	    self._db.commit()
	
	def getr(self, lrid):
		return self._r[lrid]

sharequeue = Queue()

inputs = []
outputs = []
servers = {}
udb = []

def loadConfig(fn):
	with open(fn, 'r') as f:
		inps = json.load(f)
	for (name, inp) in inps.iteritems():
		logging.info('Configuring "%s": %s' % (name, inp))
		if inp['type'] == 'db':
			kwp = dict(inp)
			del kwp['engine']
			del kwp['type']
			del kwp['mode']
			kwp.pop('serverid', None)
			if inp['engine'] == 'MySQL':
				import MySQLdb
				if 'cursorclass' in kwp:
					import MySQLdb.cursors
					kwp['cursorclass'] = getattr(MySQLdb.cursors, kwp['cursorclass'])
				kwp = {'name': name, 'dbmod': MySQLdb, 'dbconnect': lambda kwp=kwp: MySQLdb.connect(**kwp)}
			elif inp['engine'] == 'PostgreSQL':
				import psycopg2
				for v in kwp.itervalues():
					v = str(v)
				kwp = {'name': name, 'dbmod': psycopg2, 'dbconnect': lambda kwp=kwp: psycopg2.connect(**kwp)}
			else:
				raise ValueError('Unknown engine "%s"' % (inp['engine'],))
			if inp['mode'] == 'in':
				ai = DBSource(output=sharequeue, server=inp['serverid'], **kwp)
			elif inp['mode'] == 'out':
				ai = DBOutput(**kwp)
			elif inp['mode'] == 'user':
				ai = UserDBDict(**kwp)
				assert not udb
				udb.append(ai)
		elif inp['type'] == 'timeline':
			startTime = inp.get('startTime', 0)
			ai = ShareTimeline(name=name, startTime=startTime)
			if 'state' in inp:
				with open(inp['state'], 'r') as f:
					ai.load_from_file(f)
		else:
			raise ValueError('Unknown type "%s"' % (inp['type'],))
		
		if inp['mode'] == 'in':
			inputs.append(ai)
			if 'serverid' in inp:
				servers[inp['serverid']] = ai
		elif inp['mode'] == 'out':
			outputs.append(ai)
		if hasattr(ai, 'start'):
			ai.start()

loadConfig(sys.argv[1])

def ppShare(info):
	if '_' in info['username']:
		un = info['username']
		info['username'] = un[:un.index('_')]
	if udb: info['username'] = udb[0].get(info['username'])

def hackBack(startTime, fromcp, tocp, endTime = None):
	# NOTE: This could run and do nothing, if the clock was set backward past a checkpoint...
	logging.info("Backtracking %d from %s to %s..." % (startTime, fromcp, tocp))
	btbegan = time.time()
	totbt = [0]
	for (serverid, inp) in servers.iteritems():
		if serverid not in fromcp._serverData:
			# No shares from this server at all, this checkpoint
			inp.logger.info('Nothing to backtrack for "%s" at all' % (inp.name))
			continue
		sd = fromcp._serverData[serverid]
		if sd[3] < startTime:
			# No shares from this server since startTime
			inp.logger.info('Nothing to backtrack for "%s" (%d < %d)' % (inp.name, sd[3], startTime))
			continue
		lastsid = sd[1]
		inp.logger.info('Backtracking "%s"... (%d shares to check)' % (inp.name, lastsid - sd[0] + 1))
		# Fetch shares found beginning with startTime, but <= lastsid
		btc = [0]
		def f(erv, dbr):
			if not dbr:
				return
			fa = tuple(str(x[0]) for x in dbr.description)
			for row in dbr:
				share = dict( (fa[i], row[i]) for i in xrange(len(fa)) )
				share['time'] = parsetime(share['time'])
				share['server'] = serverid
				ppShare(share)
				fromcp._removeShare(**share)
				tocp.addShare(**share)
				totbt[0] += 1
				btc[0] += 1
		(dbx, dbxp) = ('and time < %s', (datetime.fromtimestamp(endTime),)) if endTime else ('', ())
		inp.dbdo('select * from shares where id <= %s and id >= %s and time >= %s ' + dbx + ' order by id desc', (lastsid, sd[0], datetime.fromtimestamp(startTime)) + dbxp, callback=f)
		inp.logger.info('Backtracked %d shares for "%s"' % (btc[0], inp.name))
		sd[3] = startTime - 1
	fromcp.endTime = startTime - 1
	logging.info('Backtrack complete (%d shares moved in %f seconds)' % (totbt[0], time.time() - btbegan))
for outp in outputs:
	outp.hackBack = hackBack

outputs[0].addCheckpoint(1310605194)
outputs[0].addCheckpoint(1311256633)

justSaw = {}
def processShare(info):
	if not info['id'] % 0x10000:
		lasttime = justSaw.get(info['server'], 0)
		thistime = time.time()
		delta = thistime - lasttime
		if delta > 0:
			GHs = float(0x10000 * 2**32) / delta / 1e9
		else:
			GHs = 0
		logging.info('Just saw "%s" shareid 0x%x at time %d (%f GH/s)' % (info['server'], info['id'], info['time'], GHs))
		justSaw[info['server']] = thistime
	ppShare(info)
	for outp in outputs:
		outp.addShare(**info)

def doCatchup():
	catchup = {}
	btimes = []
	for (serverid, inp) in servers.iteritems():
		inp.logger.info('Catching up on server "%s"...' % (inp.name,))
		lsim = ()
		lsix = None
		mycatchup = []
		for outp in outputs:
			lsi = outp.lastShareId(serverid)
			mycatchup.append( [outp, lsi] )
			lsim = min(lsim, lsi)
			lsix = max(lsix, lsi)
		mycatchup = filter(lambda x: x[1] < lsix, mycatchup)
		btime = inp.dbdo('select time from shares where id <= %s order by id desc limit 1', (lsix + 1,))
		btime = parsetime(btime[0][0]) if btime else 0
		btimes.append( (btime, inp, lsim) )
		for (outp, lsi) in mycatchup:
			outp.logger.info('Output "%s" needs to catch up from 0x%x' % (outp.name, lsi))
		if not mycatchup:
			inp.logger.info('Server "%s" will be resuming from shareid 0x%x at time %d' % (inp.name, lsix, btime))
			continue
		mycatchup.insert(0, lsix)
		catchup[serverid] = mycatchup
	
	def activatesrv(time, inp, lsi):
		inp.logger.info('Activating server "%s" from shareid 0x%x' % (inp.name, lsi))
		inp.lastid = lsi
		inp.wakeup()
	
	btimes.sort(lambda a, b: cmp(a[0], b[0]))
	timeout = 0
	
	while catchup or btimes:
		try:
			info = sharequeue.get(block=True, timeout=timeout)
		except QueueEmpty:
			if timeout:
				logging.info('No new data for 2 timmills, activating next server')
			timeout = timmill * 2
			btet = btimes[0][0]
			while btimes and btimes[0][0] == btet:
				bte = btimes.pop(0)
				activatesrv(*bte)
			continue
		while btimes and info['time'] >= btimes[0][0]:
			bte = btimes.pop(0)
			activatesrv(*bte)
		serverid = info['server']
		if serverid in catchup:
			mycatchup = catchup[serverid]
			for i in xrange(1, len(mycatchup)):
				(outp, lsi) = mycatchup[i]
				if lsi >= info['id']:
					continue
				ppShare(info)
				outp.addShare(**info)
				mycatchup[i][1] = info['id']
			lsix = mycatchup[0]
			if lsix == info['id']:
				srv = servers[info['server']]
				srv.logger.info('Server "%s" all caught up (to shareid 0x%x)' % (srv.name, lsix))
				del catchup[serverid]
		else:
			processShare(info)
	
	logging.info('Initial catchup complete')

def _RunCLI():
    import code, threading
    namespace = globals()

    def CLI():
        code.interact(banner=None, local=namespace)
    threading.Timer(0, CLI).start()
_RunCLI()

doCatchup()

# BEGIN: HACK

def get_block_time(block_hash, endof):
	if block_hash in ('', '-1'):
		return None if endof else 0
	if len(block_hash) != 64:
		# Backward compatibility: this is a time itself!
		return int(block_hash)
	if block_hash not in block_hash_dict:
		logging.warn("Don't know about block %s yet... waiting up to a timmill for it" % (block_hash,))
		timeout = time.time() + timmill
		while not (block_hash in block_hash_dict or time.time() > timeout):
			time.sleep(timmill / 16)
	blk = block_hash_dict[block_hash]
	if blk['time'] < 1311972179:
		t = blk['ntime']
	else:
		t = blk['time']
	if endof:
		t -= 1
	return t

class ThreadedTCPRequestHandler(SocketServer.BaseRequestHandler):
	def handle(self):
		r = self.request.makefile()
		self.DONE = False
		while not self.DONE:
			self.handle_i(r)
	
	def handle_i(self, r):
		v = 0
		start_block = r.readline().rstrip()
		if start_block == '':
			self.DONE = True
			return
		elif start_block == 'v1':
			v = 1
			start_block = r.readline().rstrip()
		end_block = r.readline().rstrip()
		start_time = get_block_time(start_block, False)
		end_time = get_block_time(end_block, True)
		d = outputs[0].countShares(ShareTypes.Adjusted, start_time, end_time, username=True)
		r.write("%s\n" % (len(d),))
		for a, s in d.iteritems():
			addr = udb[0].getr(a)
			addr = json.dumps(addr)[1:-1]
			r.write("%s\n" % (addr,))
			s = Number(s) / AdjShareRes
			r.write("%s\n" % (json.dumps( (s.numerator,s.denominator) ),))
		if v > 0:
			r.write("%d\n%d\n" % (start_time or -1, end_time or -1))
		r.flush()

class ThreadedTCPServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
	allow_reuse_address = True
	pass

def setup_tcp_server(bindto):
	global server
	server = ThreadedTCPServer(bindto, ThreadedTCPRequestHandler)
	server.serve_forever()

server_thread = threading.Thread(target=lambda: setup_tcp_server( ('localhost', 8989) ))
server_thread.setDaemon(True)
server_thread.start()

# END: HACK

def main():
	try:
		while True:
			info = sharequeue.get(block=True)
			processShare(info)
	except Exception, e:
		main._exc = e
		main._exc_info = sys.exc_info()
		main._exc_last = info
		raise

main()
